// añade un elemento h1 al darle al boton

$(document).on("click", "#boton1", function () {
    var a_lista = $("<h1>").text("Primer elemento");
    $("#div1").append(a_lista);
});


// añade elementos de un array

$(document).on("click", "#boton2", function () {
    var lista = ["Segundo Elemento", "Tercer elemento", "Cuarto elemento", "Quinto elemento", "Sexto elemento"];
    $(lista).each(function (index, item) {
        $("#listaItems").append("<li>" + item + "</li>");
    });
    $("#boton2").attr("disabled", "angeles");
});



// PARA AÑADIR DATOS A UNA TABLA

var listaTelefonos = [{
    nombre: "ana",
    tel: "android"
},
{
    nombre: "javi",
    tel: "iphone"
},
{
    nombre: "carmen",
    tel: "android"
},
{
    nombre: "rubén",
    tel: "android"
},
];

$(document).on("click", "#boton3", function () {
    var t_campos = "";
    $(listaTelefonos).each(function (i, index) {
        //… hacemos lo que convenga con cliente.nombre, cliente.tel...
        t_campos = "<tr><th>" + index.nombre + "</th><th>" + index.tel + "</th></tr>"; // meto en la variable los datos
        $("#t-datos").append(t_campos);

    });
    $("#boton3").attr("disabled", "angeles");
});


// PARA AÑADIR DATOS DESDE UNA BBOO

$(document).on("click", "#boton4", function () {
    var t_campos = "";
    var android = 0;
    var iphone = 0;
    var otros = 0;
    $.getJSON("json/clientes.json", function (data) {
        data.clientes.forEach(function (cliente) {
            //… hacemos lo que convenga con cliente.nombre, cliente.tel...
            t_campos = "<tr><td>" + cliente.id + "</td><td>" + cliente.nombre + "</td><td>" + cliente.tel + "</td></tr>"; // meto en la variable los datos
            $("#t-datos").append(t_campos);
            if (cliente.tel == "android") {
                android++;
            }
            else if (cliente.tel == "iphone") {
                iphone++;
            }
            else {
                otros++;
            }

        });
        var total = (android + iphone + otros);
        android = ((android * 100) / total);
        android= android.toFixed(2);
        iphone = ((iphone * 100) / total);
        iphone= iphone.toFixed(2);
        otros = ((otros * 100) / total);
        otros= otros.toFixed(2);

        t_campos = "<tr><td> </td><td>Androides</td><td>" + android + "%</td></tr>";
        $("#t-foot").append(t_campos);
        t_campos = "<tr><td> </td><td>Iphone</td><td>" + iphone + "%</td></tr>";
        console.log(t_campos);
        $("#t-foot").append(t_campos);
        t_campos = "<tr><td> </td><td>Otros</td><td>" + otros + "%</td></tr>";
        $("#t-foot").append(t_campos);
    });


    $("#boton4").attr("disabled", "angeles");
});




